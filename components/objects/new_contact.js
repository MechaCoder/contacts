import React from 'react';
import { StyleSheet, Text, View, Button, Dimensions } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';

export default class NewContact extends React.Component {

    constructor(){
        super();

        this.state = {
            "hide": false,
            "name": "",
            "tele": ""
        };

        this._onpressEvent = this._onpressEvent.bind(this);
        this._changeDataTitle = this._changeDataTitle.bind(this);
        this._changeDataTel = this._changeDataTel.bind(this);
        this._saveEvent = this._saveEvent.bind(this);
    }

    _onpressEvent(){

        let newState = true;
        if(this.state.hide){
            newState = false;
        }

        this.setState({"hide": newState})
        
    }

    _changeDataTitle(text){
        this.setState({
            "name": text
        })
    }
    _changeDataTel(text){
        this.setState({
            "tele": text
        })
    }

    _saveEvent(){
        
        if(this.state.name.length === 0){
            alert("You nee to have a name for the contact");
            return false;
        }

        if(this.state.tele.length === 0){
            alert("You nee to have a phone number for the contact");
            return false;
        }

        this.props.eventAdd(this.state.name, this.state.tele)
    }

    render(){

        let displayvalue = "flex"
        if(this.state.hide){
            displayvalue = "none"
        }

        let styleObj = {
            lable: {
                width: 100,
                // height: 50
            },
            input: {
                backgroundColor: "blue",
                color: "white",
                width: (Dimensions.get("window").width - 100)
            }
        }

        return(
            <View style={{
                marginBottom: 25,
                backgroundColor: "red"
            }}>
                <Button title="new contact" style={{width: 100}} onPress={this._onpressEvent} />
                <View style={{display: displayvalue, backgroundColor: "white"}}>
                    <View style={{flex: 1, flexDirection: 'row'}} >
                        <Text style={ styleObj.lable }>Name:</Text>
                        <TextInput 
                            style={styleObj.input}
                            value={this.state.name} 
                            onChangeText={this._changeDataTitle}
                        />
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
                        <Text style={ styleObj.lable }>Tel: </Text>
                        <TextInput 
                            style={styleObj.input}
                            value={this.state.tele}
                            onChangeText={this._changeDataTel}

                        />
                    </View>
                    <View style={{flex: 1, flexDirection: 'row', marginTop: 5}}>
                        <Button title="Save" onPress={this._saveEvent} />
                    </View>
                </View>

            </View>
        )
    }
}