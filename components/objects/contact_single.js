import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, Dimensions, Alert } from 'react-native';
import { red } from 'ansi-colors';

export default class Contact extends React.Component {

    constructor(props){
        super();

        this.state = {
            "display": "none",
            "tel": props.tel
        }

        this._onPress = this._onPress.bind(this);
        this._onPressSave = this._onPressSave.bind(this);
        this._onTextChange = this._onTextChange.bind(this);
        this._onDeletePress = this._onDeletePress.bind(this);
    }

    _onPress(){
        let display = "none"
        if (this.state.display === "none"){
            display = "flex"
        }


        this.setState({
            'display': display
        })
    }

    _onPressSave(){
        let nameToBeChanged = this.props.name;
        let newTel = this.state.tel;
        let key = this.props.keyid;

        this.props.saveFuct(
            key,
            nameToBeChanged,
            newTel
        )

    }

    _onTextChange(text){ 

        this.setState({"tel": text});

    }

    _onDeletePress(){
        Alert.alert(
            "confim",
            "aro u you sure you want to delete " + this.props.name + "?",
            [
                {text: "No thanks"},
                {text: "yes", onPress: () => {

                    this.props.deleFuct(this.props.keyid);
                    
                } }
            ]

        )

        
    }

    render(){

        return(<View style={{marginBottom: 15,}}>
            <Button
                title={this.props.name}
                onPress={this._onPress}
                style={{width: Dimensions.get('window').width, }}
            />
            <View style={{display: this.state.display,}}>
                <View style={{flex: 0, flexDirection: 'row'}}>
                <Text 
                    style={{color: "blue", marginLeft: 5, width: 50, height: 50, }}>Tel</Text> 
                <TextInput 
                    style={{backgroundColor: "blue", color: "white", padding: 10, marginLeft: 15, width: 150, height: 50}} 
                    value={this.state.tel} 
                    onChangeText={this._onTextChange}
                />
                </View>
                <View style={{flex: 0, flexDirection: 'row'}}>
                    <Button title="save" onPress={this._onPressSave} />
                    <Button title="delete" onPress={this._onDeletePress} />
                </View>
            </View>
        </View>);
    }
}