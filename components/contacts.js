import React from 'react';
import { ScrollView, StyleSheet, Text, View, Dimensions, Alert } from 'react-native';
import { AsyncStorage } from "react-native";

import Contact from './objects/contact_single';
import NewContact from './objects/new_contact';

export default class ContactList extends React.Component {

    constructor(props){
        super();

        this.state = {"users": [
        ]};

        this._addContacts = this._addContacts.bind(this);
        this._editContact = this._editContact.bind(this);
        this._delContact = this._delContact.bind(this);

        this.__storeData = this.__storeData.bind(this);
        this.__retriveData = this.__retriveData.bind(this);
    }

    componentDidMount(){
        this.__retriveData();
    }

    __storeData = async() => {
        try {
            let temp = JSON.stringify(this.state.users)
            await AsyncStorage.setItem("contacts", );
        } catch (err){
            console.error(err);
            Alert.error(err);
            
        }
    }

    __retriveData = async() => {
        try {
            const value = await AsyncStorage.getItem("contacts");
            if(value !== null){
                let t = JSON.parse(value)
                this.setState({users: t});
            }
        } catch (err){
            console.error(err);
            Alert.error(err);
        }
    }

    _addContacts(name, tel){
        let temp = this.state.users;
        temp.push({
            "name": name,
            "tel": tel
        });

        this.setState({"users": temp});
        this.__storeData()

    }

    _editContact(key, name, tel){

        let temp = this.state.users;
        temp[key].tel = tel;
        this.setState({
            "users": temp
        });

        this.__storeData();

    }

    _delContact(key){

        let temp = this.state.users;
        temp.splice(key,1);
        this.setState({users: temp});


        this.__storeData();
    }

    render(){
        let conts = [];

        for(var i=0; i <= this.state['users'].length; i++){
            let thisRow = this.state['users'][i];
            if(thisRow === undefined){
                continue;
            }

            conts.push(<Contact 
                key={i}
                keyid={i}
                name={thisRow['name']}
                tel={thisRow['tel']}
                saveFuct={this._editContact}
                deleFuct={this._delContact}
            />);
        }

        return(<ScrollView style={style.contact}>
            <NewContact eventAdd={this._addContacts} />
            {conts}
        </ScrollView>);
    }

}

const style = StyleSheet.create({
    contact: {
        marginTop: 25,
        width: Dimensions.get("window").width
    }
})