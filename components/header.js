import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';

export default class Header extends React.Component{
    render(){
        return(
            <View style={{backgroundColor: 'red', width: Dimensions.get('window').width }} >
                <Text style={{fontSize: 48, color: 'white', marginTop: 10}} >Contact List</Text>
            </View>
        );
    }
}