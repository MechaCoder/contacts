import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import ContactList from './components/contacts';
import Header from './components/header';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Header />
        <ContactList />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    // justifyContent: 'space-evenly'
  },
});
